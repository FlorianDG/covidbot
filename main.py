from data import readin_data
from botbuilder import CovidBot

try_again = True

while try_again:
    codes, w_number = readin_data()

    for row in codes.index:
        impfz = codes.at[row, "Impfzentrum"]
        validation_code = codes.at[row, "Code"]
        clicks = codes.at[row, "Klicks"]

        covidBot = CovidBot(impfz, validation_code, clicks, w_number)
        try_again = covidBot.result
