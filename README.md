# README COVIDBOT

This Repository contains the Code for a Selenium and pyhwatkit based automated Bot that automatically checks local German COVID-Vaccine Centers for available vaccination appointments. When an appointment is found, a whatsapp message is send.

### To Run 
1. Install chromedriver and replace chrome driver location `botbuilder.py`
2. Setup txt-file with phone numbers that the alert is supposed to be send
3. Setup Excel Sheet with validation codes


### Disclaimer
This Bot is no attempt to DDoS public governement pages, but rather an automization approach for a tedious manual task.
