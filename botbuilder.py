from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from time import sleep
import time
import pywhatkit


class CovidBot:

    def __init__(self, impfz, validation_code, clicks, w_number):
        self.w_numbers = w_number

        print("Started Covid-Bot: " + str(impfz))

        self.driver = webdriver.Chrome(
            executable_path='/Users/FlorianGrabe/Documents/PythonProjects/CovidBot/chromedriver')
        self.driver.get("https://www.impfterminservice.de/impftermine")

        self.driver.maximize_window()
        sleep(1)

        # Accept Cookies
        self.accept_cookies()

        self.result = self.check_availability(validation_code, clicks)

    def check_availability(self, validation_code, clicks):
        # Fill out first form with Bundesland and Impfzentrum
        self.fill_out_first_form(clicks)

        while self.check_for_waiting_room():
            sleep(30)

        # Accept cookies again if bot is in waiting room for too long
        self.accept_cookies()

        # input validation code
        self.input_validationcode(validation_code)

        # "Termin suchen" Button Klicken und evt. warten
        self.click_termin_button()

        # Process Response
        sleep(1)

        result = self.wait_for_response()

        print("Result:" + result)

        if 'keine Termine' in result:
            # close chromedriver
            self.driver.close()
            return True
        else:
            self.driver.maximize_window()
            print("IMPFTERMIN GEFUNDEN!!!!!!!")
            # Send whatsapp with message
            hour, min = map(int, time.strftime("%H %M").split())
            for number in self.w_numbers:
                pywhatkit.sendwhatmsg(number, 'IMPFTERMIN GEFUNDEN: ' + str(self.driver.current_url), hour, min + 2)

            sleep(800)
            return False

    def wait_for_response(self):
        continue_wait = True
        response_text = 'No Response'

        while continue_wait:
            try:
                response_text = self.driver.find_element_by_xpath(
                    '// *[ @ id = "itsSearchAppointmentsModal"] / div / div / div[2] / div / div / form / div[1] / span')
                continue_wait = False
            except NoSuchElementException:
                # Case: Loading takes long -> wait some more
                sleep(10)
                continue_wait = True

        result = str(response_text.get_attribute("innerHTML")).rsplit(';', 1)[1]
        return result

    def click_termin_button(self):
        try_again = True

        while try_again:
            try:
                termin_button = self.driver.find_element_by_xpath(
                    '/html/body/app-root/div/app-page-its-search/div/div/div[2]/div/div/div[5]/div/div[1]/div[2]/div['
                    '2]/button')
                termin_button.click()
                try_again = False
            except NoSuchElementException:
                sleep(10)
                try_again = True

    def accept_cookies(self):
        try:
            cookies = self.driver.find_element_by_xpath('/html/body/app-root/div/div/div/div[2]/div[2]/div/div[1]/a')
            cookies.click()
        except NoSuchElementException:
            print('Cannot find cookies')
            pass

    def input_validationcode(self, validation_code):

        sleep(1)
        # check - "Ich habe einen Vermittlungscode"
        code_button = self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-page-its-login/div/div/div['
            '2]/app-its-login-user/div/div/app-corona-vaccination/div[2]/div/div/label[1]/span')
        code_button.click()
        sleep(1)

        # Vermittlungscode Input ausfüllen
        v_input = self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-page-its-login/div/div/div['
            '2]/app-its-login-user/div/div/app-corona-vaccination/div[3]/div/div/div/div['
            '1]/app-corona-vaccination-yes/form/div[1]/label/app-ets-input-code/div/div[1]/label/input')
        v_input.send_keys(validation_code)

        # Warten vor dem Abschicken #IchBinKeinBot

        sleep(1)

        v_input.send_keys(Keys.ENTER)
        sleep(1)
        # Work around the 429 Server Error
        try:
            error_warning = self.driver.find_element_by_xpath('/html/body/app-root/div/app-page-its-login/div/div'
                                                              '/div['
                                                              '2]/app-its-login-user/div/div/app-corona-vaccination'
                                                              '/div[3]/div/div/div/div['
                                                              '1]/app-corona-vaccination-yes/form/div[1]/div')
            print('Detected 429 Server Error')
            sleep(10)
            v_input.send_keys(Keys.ENTER)
        except NoSuchElementException:
            sleep(1)

    def fill_out_first_form(self, clicks):
        # Open Dropdown and select BW
        state_dropdown = self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-page-its-center/div/div[2]/div/div/div/div/form/div['
            '3]/app-corona-vaccination-center/div[1]/label/span[2]/span[1]/span')
        state_dropdown.click()

        # Press Enter to select BW
        state_dropdown.send_keys(Keys.ENTER)

        # Select Impfzentrum
        center_dropdown = self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-page-its-center/div/div[2]/div/div/div/div/form/div['
            '3]/app-corona-vaccination-center/div[2]/label/span[2]/span[1]/span')
        center_dropdown.click()

        for i in range(0, clicks):
            center_dropdown.send_keys(Keys.ARROW_DOWN)

        center_dropdown.send_keys(Keys.ENTER)

        # submit
        submit_button = self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-page-its-center/div/div[2]/div/div/div/div/form/div[4]/button')
        submit_button.click()
        sleep(1)

    def check_for_waiting_room(self):
        continue_wait = True
        try:
            self.driver.find_element_by_xpath('/html/body/section/div[2]/div/div/h1')
            print('Currently in Line in the waiting room')

        except NoSuchElementException:
            continue_wait = False

        return continue_wait
